open cypress/e2e/spec.cy.js

## Run witch GitlabCI

export following env vars for credentials
```
CYPRESS_LOGIN=[my-login]
CYPRESS_PASSWORD=[password]
```

use ci/cd schedule from gitlab, use "0 * * * *" as expression (gitlab scheduled pipelines  cannot run more frequently than once per 60 minutes.)

wait for email that pipeline was fixed :)

## Run with docker

Build image
```
docker build -t venetian-check .
```
Run
```
docker run --rm -e CYPRESS_LOGIN=... -e CYPRESS_PASSWORD=... venetian-check:latest
```
